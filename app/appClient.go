package app

import (
	"cwtch.im/cwtch/app/plugins"
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/peer"
	"cwtch.im/cwtch/storage"
	"fmt"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"path"
	"strconv"
)

type applicationClient struct {
	applicationBridge
	appletPeers

	appBus event.Manager
}

// NewAppClient returns an Application that acts as a client to a AppService, connected by the IPCBridge supplied
func NewAppClient(appDirectory string, bridge event.IPCBridge) Application {
	appClient := &applicationClient{appletPeers: appletPeers{peers: make(map[string]peer.CwtchPeer)}, applicationBridge: applicationBridge{applicationCore: *newAppCore(appDirectory), bridge: bridge}, appBus: event.NewEventManager()}
	appClient.handle = appClient.handleEvent

	go appClient.listen()

	appClient.bridge.Write(&event.IPCMessage{Dest: DestApp, Message: event.NewEventList(event.ReloadClient)})

	log.Infoln("Created new App Client")
	return appClient
}

// GetPrimaryBus returns the bus the Application uses for events that aren't peer specific
func (ac *applicationClient) GetPrimaryBus() event.Manager {
	return ac.appBus
}

func (ac *applicationClient) handleEvent(ev *event.Event) {
	switch ev.EventType {
	case event.NewPeer:
		localID := ev.Data[event.Identity]
		password := ev.Data[event.Password]
		reload := ev.Data[event.Status] == "running"
		ac.newPeer(localID, password, reload)
	case event.PeerError:
		ac.appBus.Publish(*ev)
	case event.AppError:
		ac.appBus.Publish(*ev)
	case event.ACNStatus:
		ac.appBus.Publish(*ev)
	case event.ReloadDone:
		ac.appBus.Publish(*ev)
	}
}

func (ac *applicationClient) newPeer(localID, password string, reload bool) {
	profile, err := storage.ReadProfile(path.Join(ac.directory, "profiles", localID), password)
	if err != nil {
		log.Errorf("Could not read profile for NewPeer event: %v\n", err)
		ac.appBus.Publish(event.NewEventList(event.PeerError, event.Error, fmt.Sprintf("Could not read profile for NewPeer event: %v\n", err)))
		return
	}

	_, exists := ac.peers[profile.Onion]
	if exists {
		log.Errorf("profile for onion %v already exists", profile.Onion)
		ac.appBus.Publish(event.NewEventList(event.PeerError, event.Error, fmt.Sprintf("profile for onion %v already exists", profile.Onion)))
		return
	}

	eventBus := event.NewIPCEventManager(ac.bridge, profile.Onion)
	peer := peer.FromProfile(profile)
	peer.Init(eventBus)

	ac.mutex.Lock()
	defer ac.mutex.Unlock()
	ac.peers[profile.Onion] = peer
	ac.eventBuses[profile.Onion] = eventBus
	npEvent := event.NewEvent(event.NewPeer, map[event.Field]string{event.Identity: profile.Onion})
	if reload {
		npEvent.Data[event.Status] = "running"
	}
	ac.appBus.Publish(npEvent)

	if reload {
		ac.bridge.Write(&event.IPCMessage{Dest: DestApp, Message: event.NewEventList(event.ReloadPeer, event.Identity, profile.Onion)})
	}
}

// CreatePeer messages the service to create a new Peer with the given name
func (ac *applicationClient) CreatePeer(name string, password string) {
	log.Infof("appClient CreatePeer %v\n", name)
	message := event.IPCMessage{Dest: DestApp, Message: event.NewEvent(event.CreatePeer, map[event.Field]string{event.ProfileName: name, event.Password: password})}
	ac.bridge.Write(&message)
}

func (ac *applicationClient) AddPeerPlugin(onion string, pluginID plugins.PluginID) {
	message := event.IPCMessage{Dest: DestApp, Message: event.NewEvent(event.AddPeerPlugin, map[event.Field]string{event.Identity: onion, event.Data: strconv.Itoa(int(pluginID))})}
	ac.bridge.Write(&message)
}

// LoadProfiles messages the service to load any profiles for the given password
func (ac *applicationClient) LoadProfiles(password string) {
	message := event.IPCMessage{Dest: DestApp, Message: event.NewEvent(event.LoadProfiles, map[event.Field]string{event.Password: password})}
	ac.bridge.Write(&message)
}

func (ac *applicationClient) QueryACNStatus() {
	message := event.IPCMessage{Dest: DestApp, Message: event.NewEvent(event.GetACNStatus, map[event.Field]string{})}
	ac.bridge.Write(&message)
}

// ShutdownPeer shuts down a peer and removes it from the app's management
func (ac *applicationClient) ShutdownPeer(onion string) {
	ac.mutex.Lock()
	defer ac.mutex.Unlock()
	ac.eventBuses[onion].Shutdown()
	delete(ac.eventBuses, onion)
	ac.peers[onion].Shutdown()
	delete(ac.peers, onion)
	message := event.IPCMessage{Dest: DestApp, Message: event.NewEvent(event.ShutdownPeer, map[event.Field]string{event.Identity: onion})}
	ac.bridge.Write(&message)
}

// Shutdown shuts down the application client and all front end peer components
func (ac *applicationClient) Shutdown() {
	for id := range ac.peers {
		ac.ShutdownPeer(id)
	}
	ac.applicationBridge.Shutdown()
	ac.appBus.Shutdown()
}
