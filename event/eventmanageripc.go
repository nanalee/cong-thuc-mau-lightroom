package event

type ipcManager struct {
	manager Manager

	onion     string
	ipcBridge IPCBridge
}

// NewIPCEventManager returns an EvenetManager that also pipes events over and supplied IPCBridge
func NewIPCEventManager(bridge IPCBridge, onion string) Manager {
	em := &ipcManager{onion: onion, ipcBridge: bridge, manager: NewEventManager()}
	return em
}

// IPCEventManagerFrom returns an IPCEventManger from the supplied manager and IPCBridge
func IPCEventManagerFrom(bridge IPCBridge, onion string, manager Manager) Manager {
	em := &ipcManager{onion: onion, ipcBridge: bridge, manager: manager}
	return em
}

func (ipcm *ipcManager) Publish(ev Event) {
	ipcm.manager.Publish(ev)
	message := &IPCMessage{Dest: ipcm.onion, Message: ev}
	ipcm.ipcBridge.Write(message)
}

func (ipcm *ipcManager) PublishLocal(ev Event) {
	ipcm.manager.Publish(ev)
}

func (ipcm *ipcManager) Subscribe(eventType Type, queue Queue) {
	ipcm.manager.Subscribe(eventType, queue)
}

func (ipcm *ipcManager) Shutdown() {
	ipcm.manager.Shutdown()
}
