package groups

import (
	"crypto/rand"
	"cwtch.im/cwtch/event"
	"cwtch.im/cwtch/peer"
	"golang.org/x/crypto/nacl/secretbox"
	"io"
)

// Fuzz various group related functions
func Fuzz(data []byte) int {
	peer := peer.NewCwtchPeer("fuzz")
	peer.Init(event.NewEventManager())

	inviteid, err := peer.GetProfile().ProcessInvite(string(data), peer.GetProfile().Onion)

	if err != nil {
		if inviteid != "" {
			panic("should not have added a group on err")
		}
		return 1
	}

	id, _, _ := peer.StartGroup("2c3kmoobnyghj2zw6pwv7d57yzld753auo3ugauezzpvfak3ahc4bdyd")
	var nonce [24]byte
	io.ReadFull(rand.Reader, nonce[:])
	encrypted := secretbox.Seal(nonce[:], data, &nonce, &peer.GetGroup(id).GroupKey)
	ok, _, _, _ := peer.GetProfile().AttemptDecryption(encrypted, data)
	if ok {
		panic("this probably shouldn't happen")
	}
	ok = peer.GetProfile().VerifyGroupMessage(string(data), string(data), string(data), 0, encrypted, data)
	if ok {
		panic("this probably shouldn't happen")
	}
	return 0
}
